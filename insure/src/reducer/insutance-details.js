export default function(state = null, action){
    switch(action.type){
        case "INSURANCE":
            console.log("STATE IS: "+JSON.stringify(state));
            return {...state,data: action.payload.insuranse, fetched: true, connection: true, imgData: action.payload.imgData, face: true};
        case "FETCH":
            return {...state,fetched: false, connection: true, face: true};
        case "NO_DATA":
            return {...state,fetched: true, connection: false, face: true};
        case "LARGE_FILE":
            return {fetched: true, connection: true, large: true, face: true};
        case "NO_FACE":
            return {fetched: true, connection: true, face: false};
        // case "UPDATE_PROP":
        //     return {...state, fetched: true, connection: true, imgData: action.payload.imgData};
        default:
            break;
    }
    return state
}