export default function(state = null, action){
    switch(action.type){
        case "IMAGE_FATCH":
            console.log("imgData"+action.payload);
            return {data: action.payload, fetched: true, connection: true}
        case "FETCHING":
            return {fetched: false, connection: true}
        case "LARGE_FILE":
            return {fetched: false, connection: true, large: true};
        case "NO_FACE":
            return {fetched: true, connection: true, face: false};
        case "NO_DATAS":
            return {fetched: false, connection: false}
        default:
            break;
    }
    return state
}