import axios from 'axios';

export const selectUser = (user) => {
    console.log(user.name);
    return{
        type: "USER_SELECTED",
        payload: user
    }
}

export const getDeals = (url, imgdata) => {

    console.log(url);
    return(dispatch) => {
        dispatch({type: "FETCH"});
        return axios.get(url).then((res)=>{
            // if(res.data){
            //     sessionStorage.setItem('DEALS',JSON.stringify(res.data));
            // }
            console.log("response is"+JSON.stringify(res));
            dispatch(
                {
                    type: "INSURANCE",
                    payload: {insuranse: res.data, imgData: imgdata}
                }
            );
        }).catch(err => {
            console.log("no data");
            dispatch({type: "NO_DATA"});
        })
    }
}

export const getImageDetails = (obj) => {
    return(dispatch) => {
        dispatch({type: "FETCH"});
        return axios.post("http://localhost:8000/getData", obj).then(
            response => {
                let res = JSON.parse(response.data);
                console.log("Hi"+JSON.stringify(res));
                if(JSON.stringify(res).indexOf("413 Request Entity Too Large") > -1){
                    dispatch({type: "LARGE_FILE"})
                }else{
                    if(res.faces){
                        if(res.faces.length === 0){
                            dispatch({type: "NO_FACE"})
                        }else{
                            let age = res.faces[0].attributes.age.value;
                            console.log(age)
                            if(age >= 20 && age <= 25){
                                res.faces[0].attributes.age.value = "20-25";
                            }
                            else if(age >= 26 && age <= 30){
                                res.faces[0].attributes.age.value = "26-30";
                            }
                            else if(age >= 31 && age <= 35){
                                res.faces[0].attributes.age.value = "31-35";
                            }
                            else if(age >= 36 && age <= 40){
                                res.faces[0].attributes.age.value = "36-40";
                            }
                            else if(age >= 41 && age <= 45){
                                res.faces[0].attributes.age.value = "41-45";
                            }
                            else if(age >= 46 && age <= 50){
                                res.faces[0].attributes.age.value = "46-50";
                            }
                            else if(age >= 51 && age <= 51){
                                res.faces[0].attributes.age.value = "50-55";
                            }
                            console.log("Image found");
                            let url = 'http://localhost:8088/fetch/insurance/'+res.faces[0].attributes.age.value+'/0';
                            return axios.get(url).then((re) => {
                                console.log("insurance data: " + re.data);
                                dispatch(
                                    {
                                        type: "INSURANCE",
                                        payload:{insuranse: re.data, imgData: res}
                                    }
                                );
                            }).catch(err => {
                                console.log(err);
                                dispatch({type: "NO_DATAS"});
                            })
                        }
                    }else{
                        console.log("Name");
                        dispatch({type: "TECH_ERROR"});
                    }
                }
            }
        ).catch(err => {
            console.log(err);
            dispatch({type: "NO_DATAS"});
        })
    }
}

export const login = (response) => {
    console.log("loging in ");
    return(dispatch) => {
        localStorage.setItem("USER", JSON.stringify(response));
        let obj = {
            name: response.name,
            uniqueId: response.id,
            imgUrl: response.picture.data.url
        }
        axios.post("http://localhost:8088/login/user", obj).then((res) => {
            localStorage.setItem("liked", res.data.likedInsurance);
            dispatch(
                {
                    type: "LOGIN",
                    payload: {data: true, name: response.name, liked: res.data.likedInsurance}
                }
            )
        }).catch((err) => {
            console.log(err);
        })
        
    }
}

export const logout = () => {
    console.log("loging out");
    return(dispatch) => {
        localStorage.clear();
        return (
            dispatch(
                {
                    type: "LOGOUT",
                    payload: false
                }
            )
        )
    }
}

export const setName = (nam,likedInsurance) => {
    return(dispatch) => {
        return(
            dispatch(
                {
                    type: "NAME",
                    payload: {data: true, name: nam, liked: likedInsurance}
                }
            )
        )
    }
}

export const likedInsurance = (id,nam,url) => {
    return(dispatch) => {
        return(
            axios.post(url,id).then((res) => {  
                localStorage.setItem("liked", res.data.likedInsurance);
                dispatch(
                    {
                        type: "NAME",
                        payload: {data: true, name: nam, liked: res.data.likedInsurance}
                    }
                )
            }).catch((err) => console.log(err))
        )
    }
}



