import React from 'react';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Insur from '../containers/Insur'
import InsuranceDeal from '../containers/InsuraceDeals';
import Header from '../containers/header'

const App = () => (
    <MuiThemeProvider>
    <div>
        <Header/>
        <br/><br/><br/><br/>
        <Insur/>
        <br/>
        <InsuranceDeal/>
    </div>
    </MuiThemeProvider>
);

export default App;