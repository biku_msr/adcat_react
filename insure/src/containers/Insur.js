import React, { Component } from 'react';

// import RaisedButton from 'material-ui/RaisedButton';
import {Card, CardMedia, CardTitle} from 'material-ui/Card';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

// import logo from '../logo.svg';
import selfie from '../static/selfieAnimation.gif';
import loaderImage from '../static/loader.gif';
import '../App.css';
import {connect} from 'react-redux';
import {selectUser} from '../action/index';
import {getDeals} from '../action/index';
import {getImageDetails} from '../action/index';
import {setName} from '../action/index';

import { bindActionCreators } from 'redux';


class Insur extends Component {

  constructor(props) {
    super(props)
    this.state = {
      files: "",
      name: "",
      age: "",
      gender: "",
      income: "",
      showModal: false
    }
      
  }

  createListItem(){
    return this.props.users.map((user,i) => {
      return (
        <li key={i} onClick={() => this.props.selectUser(user)}>{user.name}</li>
      );
    })
  }

  imageUpload(event){
    if(localStorage.getItem('USER')){
      this.props.setName(JSON.parse(localStorage.getItem('USER')).name);
    }
    var reader = new FileReader();
    if(event.target.files && event.target.files[0]){
      reader.readAsDataURL(event.target.files[0]); // read file as data url
        //this.fileChange(event);
        reader.onload = (event) => { // called once readAsDataURL is completed
          let url = event.target.result;
          this.setState({ files: url });
          this.props.getImageDetails({base64: url, name: this.state.name});
          // console.log(url);
        }
    }else{
      this.setState({ files: "" })
    }
    
  }

  renderWhenNoResponse(){
    return(
      <div>
      <div className="col-lg-12 col-md-12 col-sm-12 hidden-xs" style= {{textAlign: "center"}}>
        <div className="col-lg-4 col-md-3 col-sm-3"></div>
        <div className="col-lg-4 col-md-6 col-sm-6">
          <form >
            <Card style={{ width: "100%", margin: 0}} className="pointerCursor" onClick={() => document.getElementById("selfieImg").click()}>
              <CardMedia overlay={<CardTitle title={this.state.files.length > 0 ? "Want to take again" :"Take a Selfie"} subtitle="Get the best Quote for you" />}>
                  <img src={this.state.files.length > 0 ? this.state.files : selfie }  alt="" height="300px"/>
              </CardMedia>
            </Card>
           
            <input style={{display: "none"}} id="selfieImg" accept="image/*" capture="camera" type="file" onChange={(event)=> { 
               this.imageUpload(event);
          }}/>
          </form>
        </div>
        <div className="col-lg-4 col-md-3 col-sm-3"></div>
      </div>

      <div className="col-xs-12 visible-xs zeroPadding" style= {{textAlign: "center"}}>
          <Card style={{ width: "100%", maxHeight: 400}} className="pointerCursor" onClick={() => document.getElementById("selfieImg").click()}>
              <CardMedia overlay={<CardTitle title={this.state.files.length > 0 ? "Want to take again" :"Take a Selfie"} subtitle="Get the best Quote for you" />}>
                  <img src={this.state.files.length > 0 ? this.state.files : selfie }  alt="" height="300px"/>
              </CardMedia>
            </Card>
           
            <input style={{display: "none"}} id="selfieImg" accept="image/*"  capture="camera" type="file" onChange={(event)=> { 
               this.imageUpload(event);
          }}/>
      </div>
    </div>
    );
  }

  renderWhenResponse(){
    return(
      
      <div className="col-md-12 zeroPadding">
        
        <div className="col-xs-12 visible-xs zeroPadding" style= {{textAlign: "center"}}>
            <Card style={{ width: "100%", maxHeight: 400}} className="pointerCursor" onClick={() => document.getElementById("selfieImg").click()}>
                <CardMedia overlay={<CardTitle title={this.state.files.length > 0 ? "Want to take again" :"Take a Selfie"} subtitle="Get the best Quote for you" />}>
                    <img src={this.state.files.length > 0 ? this.state.files : selfie }  alt="" height="300px"/>
                </CardMedia>
              </Card>
            
              <input style={{display: "none"}} id="selfieImg" accept="image/*" capture="camera" type="file" onChange={(event)=> { 
                this.imageUpload(event);
            }}/>
        </div>
        <div className="col-md-3 col-sm-3 col-lg-3 hidden-xs" style= {{textAlign: "center"}}>
            <Card style={{ width: "100%", maxHeight: 400}} className="pointerCursor" onClick={() => document.getElementById("selfieImg").click()}>
                <CardMedia overlay={<CardTitle title={this.state.files.length > 0 ? "Want to take again" :"Take a Selfie"} subtitle="Get the best Quote for you" />}>
                    <img src={this.state.files.length > 0 ? this.state.files : selfie }  alt="" height="300px"/>
                </CardMedia>
              </Card>
            
              <input style={{display: "none"}} id="selfieImg"  accept="image/*"  capture="camera" type="file" onChange={(event)=> { 
                this.imageUpload(event);
            }}/>
        </div>
        <div className="col-md-9 col-xs-12" style={{ textAlign: "left"}}>
          <TextField
            style={{width: "70%"}}
            value={this.props.auth.name}
            onChange={this.setName.bind(this)}
            floatingLabelText="Name"
          />
          <SelectField
            style={{width: "70%"}}
            floatingLabelText="age"
            value={this.props.deal.imgData.faces[0].attributes.age.value}
            onChange={this.changeAge}
          >
           {this.getAges()}
          </SelectField><br/>

          <SelectField
            style={{width: "70%"}}
            floatingLabelText="Gender"
            value={this.props.deal.imgData.faces[0].attributes.gender.value}
            onChange={this.changeGender}
          >
           <MenuItem value="Male" primaryText="Male" />
           <MenuItem value="Female" primaryText="Female" />
           <MenuItem value="Other" primaryText="Other" />
          </SelectField>

          <SelectField
            style={{width: "70%"}}
            floatingLabelText="Income per annum"
            value={this.state.income}
            onChange={this.changeIncome}
          >
           <MenuItem value="3-5" primaryText="3L-5L" />
           <MenuItem value="5-7" primaryText="5L-7L" />
           <MenuItem value="7-10" primaryText="7L-10L" />
           <MenuItem value="10+" primaryText="More then 10" />
          </SelectField>
        </div>
      </div>
    );
  }

  nameRender(){
    if(this.props.auth){
      return(
        <TextField
          style={{width: "70%"}}
          value={this.props.auth.name}
          onChange={this.setName.bind(this)}
          floatingLabelText="Name"
        />
      );
    }else{
      return(
        <TextField
          style={{width: "70%"}}
          value=""
          onChange={this.setName.bind(this)}
          floatingLabelText="Name"
        />
      );
    }
  }

  getAges(){
    return ["20-25","26-30","31-35","36-40","41-45","46-50","51-55"].map(
      (age,i) => {
        // console.log(age);
        return(
          <MenuItem key={i} value={age} primaryText={age} />
        );
      })
  }

  setName(event){
    // console.log(event.target.value);
    this.setState({ name: event.target.value });
    this.props.setName(event.target.value);
  }

  changeAge = (event, index, value) => {
    let income = "0";
    this.setState({ age : value});
    this.props.deal.imgData.faces[0].attributes.age.value = value;
    if(this.state.income.length > 0){
      income = this.state.income;
    }
    this.props.getDeals('http://localhost:8088/fetch/insurance/'+value+'/'+income, this.props.deal.imgData);
  };
  changeGender = (event, index, value) => {
    let income = "0";
    this.setState({ showModal: true });
    this.setState({ gender: value });
    this.props.deal.imgData.faces[0].attributes.gender.value = value;
    if(this.state.income.length > 0){
      income = this.state.income;
    }
    this.props.getDeals('http://localhost:8088/fetch/insurance/'+this.props.deal.imgData.faces[0].attributes.age.value+'/'+income, this.props.deal.imgData);
    this.setState({ showModal: false });
  };
  changeIncome = (event, index, value) => {
    this.setState({ income: value });
    this.props.getDeals('http://localhost:8088/fetch/insurance/'+this.props.deal.imgData.faces[0].attributes.age.value+'/'+value, this.props.deal.imgData);
  };


  viewLoader(){
    // console.log("hi");
    return(
      <div style={{zIndex: 1000000}} className="overlay">
          <img style={{marginTop: "11%", marginLeft: "30%"}} className="hidden-xs" alt="" src={loaderImage}/>  
          <img style={{marginTop: "61%", marginLeft: "4%"}} className="visible-xs" alt="" height="30%" src={loaderImage}/>    
      </div>
    );
  }

  loadImageData(){
    console.log("logding data");
    if(this.props.users && this.props.users.data.faces){
      let age = this.props.users.data.faces[0].attributes.age.value;
      console.log(age)
      if(age >= 20 && age <= 25){
          age = "20-25";
      }
      else if(age >= 26 && age <= 30){
          age = "26-30";
      }
      else if(age >= 31 && age <= 35){
          age = "26-30";
      }
      else if(age >= 36 && age <= 40){
          age = "26-30";
      }
      else if(age >= 41 && age <= 45){
          age = "26-30";
      }
      else if(age >= 46 && age <= 50){
          age = "26-30";
      }
      else if(age >= 51 && age <= 51){
          age = "26-30";
      }
    this.setState({age: age});
    this.setState({gender: this.props.users.data.faces[0].attributes.gender.value});
    let url = 'http://localhost:8088/fetch/insurance/'+age+'/0';
    this.props.getDeals(url);
  }
}

  render(){
    return (
      <div className="col-xs-12 zeroPadding">
        
        {/* {!this.props.users || !this.props.users.fetched ? this.renderWhenNoResponse() : this.renderWhenResponse()} */}
        {!this.props.deal || !this.props.deal.connection || this.props.deal.imgData == null ? this.renderWhenNoResponse() : this.renderWhenResponse()}

        {this.props.deal && !this.props.deal.connection ? <div style={{textAlign: "center"}}><h1>Networks down Please try after Sometime</h1></div> : <span/>}
        {this.props.deal && !this.props.deal.connection && this.props.deal.large ? <div style={{textAlign: "center"}}><h1>The image is too large to annalised try another one</h1></div> : <span/>}
        {this.props.deal && !this.props.deal.face ? <div style={{textAlign: "center"}}><h1>Please try with an another image</h1></div> : <span/>}
        
        {/* <RaisedButton label="Check" primary={true} onClick={() => this.props.getDeals('http://localhost:8088/fetch/insurance/31-35/0')} /> */}
        {this.props.deal && !this.props.deal.fetched && this.props.deal.connection ? this.viewLoader(): <span/>}
        {/* {this.props.users && !this.props.users.fetched  ? this.viewLoader(): <span/>} */}
        {/* {this.props.users && this.props.users.data ? this.loadImageData(): <span/>} */}
      </div>
      // <div className="App">
      //   <header className="App-header">
      //     <img src={logo} className="App-logo" alt="logo" />
      //     <h1 className="App-title">Welcome to React</h1>
      //   </header>
      //   <p className="App-intro">
      //     The insurance for all.
      //   </p>
      //   <ul>
      //     {this.createListItem()}
      //   </ul>
        
      // </div>
    );
  }
}

function mapStateToProps(state){
  return {
    users: state.users,
    deal: state.insurance,
    auth: state.auth
  }
}

function mapDispachToProps(dispatch){
  return bindActionCreators({selectUser: selectUser, getDeals: getDeals, getImageDetails: getImageDetails, setName:setName}, dispatch);
}

export default connect(mapStateToProps, mapDispachToProps)(Insur);
