import React, { Component } from 'react';
import {connect} from 'react-redux';

import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';

import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
// import NavigationClose from 'material-ui/svg-icons/navigation/close';
import { bindActionCreators } from 'redux';

import {login} from '../action/index';
import {logout} from '../action/index';

import FacebookLogin from 'react-facebook-login';
import {facebook} from '../static/facebook.ico';
// import TiSocialFacebookCircular from 'react-icons/lib/ti/social-facebook-circular';


// class Login extends Component {
//     static muiName = 'FlatButton';
  
//     render() {
//       return (
//         <FlatButton {...this.props} label="Login" onClick={() => this.props.login()}/>
//       );
//     }
//   }
  
//   const Logged = (props) => (
//     <IconMenu
//       {...props}
//       iconButtonElement={
//         <IconButton><MoreVertIcon /></IconButton>
//       }
//       targetOrigin={{horizontal: 'right', vertical: 'top'}}
//       anchorOrigin={{horizontal: 'right', vertical: 'top'}}
//     >
//       <MenuItem primaryText="Refresh" />
//       <MenuItem primaryText="Help" />
//       <MenuItem primaryText="Sign out" onClick={() => this.props.logout()}/>
//     </IconMenu>
//   );
  
//   Logged.muiName = 'IconMenu';


class Header extends Component {
    
    handleClick = () => {
        alert('onClick triggered on the title component');
    }

    responseFacebook = (response) => {
        console.log(response);
        this.props.login(response);
    }


    loginButtion = () => {
        // return (<FlatButton label="Login" onClick={() => this.fbLogin()}/>);
        return (
            <FacebookLogin
                appId="107244600044139"
                autoLoad={false}
                fields="name,email,picture"
                callback={this.responseFacebook}
                icon={facebook}
            />
          );
        
    }

    logedInButttion = () => {
        return(
        <span>
            <img alt="" className="img img-circle" style={{marginTop: -15}} src={JSON.parse(localStorage.getItem('USER')).picture.data.url}/> &nbsp;
            <IconMenu
            iconButtonElement={
              <IconButton><MoreVertIcon /></IconButton>
            }
            targetOrigin={{horizontal: 'right', vertical: 'top'}}
            anchorOrigin={{horizontal: 'right', vertical: 'top'}}
          >
            {/* <MenuItem primaryText="Refresh" />
            <MenuItem primaryText="Help" /> */}
            <MenuItem primaryText="Sign out" onClick={() => this.props.logout()}/>
          </IconMenu>
          </span>
        );
    }

    render() {
        return(
            <AppBar style={{position: "fixed", zIndex: 600, width: "100%"}}
            title="Insure"
            // iconElementLeft={<IconButton></IconButton>}
            // iconElementRight={this.props.auth ? <img alt="" className="img img-circle" src={JSON.parse(localStorage.getItem('USER')).picture.data.url}/>: <span />}
            iconElementRight={this.props.auth && (this.props.auth.data || localStorage.getItem('USER')) ? this.logedInButttion() : this.loginButtion()}
          />
        )
    }
}
function mapStateToProps(state){
    return {
        auth: state.auth,
        user: state.users
    }
}

function mapDispachToProps(dispatch){
    return bindActionCreators({login: login, logout: logout}, dispatch);
  }
export default connect(mapStateToProps, mapDispachToProps)(Header);