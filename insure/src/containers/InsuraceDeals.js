import React, { Component } from 'react';
import {connect} from 'react-redux';

import {Card, CardActions, CardHeader, CardMedia, CardTitle} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import {blue500, red500} from 'material-ui/styles/colors';

import Star from 'material-ui/svg-icons/toggle/star';

import {likedInsurance} from '../action/index'
import { bindActionCreators } from 'redux';

export class InsuranceDeal extends Component{

    renderInsuranceDeal(insurances){
        console.log(insurances);
        if(insurances.fetched && insurances.data){
            return insurances.data.map((insurance,i) => {
                return (
                <div key={i} className="col-md-3 col-xs-12" style={{paddingRight: 4, paddingLeft: 4}}>
                    <Card>
                        <CardHeader
                        title={insurance.insuranceName.length > 21 ? insurance.insuranceName.slice(0,20)+'...': insurance.insuranceName}
                        subtitle={insurance.ageGroup+' years'}
                        avatar={insurance.companyUrl}
                        />
                        <CardMedia
                        overlay={<CardTitle title={'Premium: '+insurance.premiumPerMonth+'/Month'} subtitle={'Policy term:'+insurance.policyTerm+' Years'} />}
                        >
                        <img src={insurance.companyUrl} alt="" height="300px"/>
                        </CardMedia>
                    
                        <CardActions>
                        <FlatButton label="Know More" href={insurance.companyLink}/>
                        {/* <FlatButton label="Learn" /> */}

                        {localStorage.getItem("liked") ? <Star style={{marginLeft: "46%"}} 
                            color={localStorage.getItem("liked").split(',').indexOf(insurance.insuranceId.toString()) > -1 || (this.props.user && this.props.user.liked && this.props.user.liked.split(',').indexOf(insurance.insuranceId.toString()) > -1) ? red500 : blue500}
                            onClick={() => this.likeInsuranse(insurance)}
                            /> : <span/>}
                        />
                        
                        {/* <FontIcon
                            className="muidocs-icon-action-home"
                            color={localStorage.getItem("liked").split(',').indexOf(insurance.insuranceId) ? red500 : blue500}
                            onClick={() => this.likeInsuranse(insurance)}
                            /> : <span/>} */}
                        </CardActions>
                    </Card>
                    <br/>
                </div>
                
                );
            })
        }
    }

    likeInsuranse(insuranse){
        
        console.log("insurance id: "+insuranse.insuranceId);
        let url = "http://localhost:8088/login/user/"+JSON.parse(localStorage.getItem("USER")).id;
        this.props.likedInsurance(insuranse.insuranceId,JSON.parse(localStorage.getItem("USER")).name,url);
    }

    render(){
        if(this.props.deal){
            return(
                <div className="col-md-12" style={{marginTop: 20}}>
                    <br/>
                    {/* <h1>{this.props.user.name}</h1> */}
                    {this.props.deal ? this.renderInsuranceDeal(this.props.deal): <div/>}
                </div>
            );
        }else{
            return(
                <div></div>
            );
        }
    }
}

function mapStateToProps(state){
    return {
      user: state.auth,
      deal: state.insurance
    }
}

function mapDispachToProps(dispatch){
    return bindActionCreators({likedInsurance: likedInsurance}, dispatch);
  }

export default connect(mapStateToProps, mapDispachToProps)(InsuranceDeal);