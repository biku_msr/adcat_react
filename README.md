There are three projects:
	1. insure (React)
	2. faceplus-insure (for fetching image data built in node.js)
	3. health (backend spring boot application)

Database is MySql

Jar file(insure.jar) you can directly run that by java -jar <fileName>.jar (The db username and password are set as root).

insuranceData.sql is the data set.

if your db username and password is different than root then please follow the below steps.
	
	1. Go to health\src\main\resources\application.properties
	2. spring.datasource.url = jdbc:mysql://localhost:3306/insurance (insurance is the db name if not same change it or create one)
	3. spring.datasource.username = root
	   spring.datasource.password = root (change according to your local configuration)
	4. import all data from insuranceData.sql to DB
	5. run the project as maven install it will generate a jar u can use that and skip step 6.
	6. got to HealthApplication.java then run as java application.
	7. its done application will stared on 8088 port
	
for insure run npm install and then npm start

for faceplus-insure run npm install and then nodemon

