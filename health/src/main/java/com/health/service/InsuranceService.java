package com.health.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.health.entity.Insurance;
import com.health.repository.InsuranceRepository;

@Service
public class InsuranceService {
	@Autowired
	private InsuranceRepository insuranceRepo;

	/*public String ImageResponse(MultipartFile file){
		//ResponseEntity<String> myObject = null;
		try{
			File file1 = new File(file.getOriginalFilename());
			file.transferTo(file1);
			HttpRequests httpRequests = new HttpRequests("AuEdYjfiG_SdnPubcUZw7UZPTzMr6qSZ", 
					"YnzwAuRAXMTPSuIiJO0uNX_HZs2nGqim");
			PostParameters postParameters = new PostParameters().setImg(file1).setAttribute("all");
			FaceppResult result = httpRequests.detectionDetect(postParameters);
			System.out.println(result.get("face").getCount());
					//.setUrl("http://faceplusplus.com/static/img/demo/8.jpg").setAttribute("all");
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);

			MultiValueMap<String, Object> map= new LinkedMultiValueMap<String, Object>();
			map.add("api_secret", "YnzwAuRAXMTPSuIiJO0uNX_HZs2nGqim");
			map.add("api_key", "AuEdYjfiG_SdnPubcUZw7UZPTzMr6qSZ");
			map.add("image_file", file);

			HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
			
			Request request = new Request();
			request.setImage_file(file);
			final String uri = "https://api-us.faceplusplus.com/facepp/v3/detect?return_attributes=gender,age";
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
		    headers.setAccept(Arrays.asList(MediaType.MULTIPART_FORM_DATA));
		    HttpEntity<Request> entity = new HttpEntity<Request>(request, headers);
			myObject = restTemplate.exchange( uri,HttpMethod.POST, entity , String.class );
			System.out.println(myObject);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}*/
	
	public List<Insurance> insurancebyAge(String ageGroup){
		List<Insurance> insurances = new ArrayList<>();
		try{
			insurances = insuranceRepo.findByAgeGroup(ageGroup);
			Collections.sort(insurances, Insurance.premiumPerMonthComparator);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return insurances;
	}
	
	public List<Insurance> insurancebyAgeAndSalary(String ageGroup, String salaryGroup){
		
		List<Insurance> insurances = new ArrayList<>();
		try{
			insurances = insuranceRepo.findByAgeGroupAndSalaryGroup(ageGroup, salaryGroup);
			Collections.sort(insurances, Insurance.premiumPerMonthComparator);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return insurances;
		
	}
}

