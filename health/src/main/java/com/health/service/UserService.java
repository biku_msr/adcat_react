package com.health.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.health.entity.User;
import com.health.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	public User getUserDetails(User user){
		User userdb = new User();
		try{
			userdb = userRepository.findByUniqueId(user.getUniqueId());
			if(null == userdb){
				User userd = new User();
				userd.setName(user.getName());
				userd.setUniqueId(user.getUniqueId());
				userd.setLikedInsurance("");
				userRepository.saveAndFlush(userd);
				return userd;
			}
			
			return userdb;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return userdb;
	}
	
	public User getUserLike(Integer insuranceId, String uniqueId){
		User userdb = new User();
		try{
			userdb = userRepository.findByUniqueId(uniqueId);
			List<String> list = new ArrayList<String>(Arrays.asList(userdb.getLikedInsurance().split(",")));
			
			if(null != userdb.getLikedInsurance() && Arrays.asList(userdb.getLikedInsurance().
					split(",")).contains(insuranceId.toString())){
//				List<String> list = Arrays.asList(userdb.getLikedInsurance().split(","));
				list.remove(insuranceId.toString());
			}
			else{
				list.add(insuranceId.toString());
			}
			String likedInsurnce = list.stream().map(Object::toString)
                    .collect(Collectors.joining(","));
			userdb.setLikedInsurance(likedInsurnce);
			userRepository.saveAndFlush(userdb);
			
			return userdb;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return userdb;
	}


}
