package com.health.entity;

import java.util.Comparator;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class Insurance {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer insuranceId;
	private String insuranceName;
	private String ageGroup;
	private String salaryGroup;
	private Integer premiumPerMonth;
	private String companyUrl;
	private String companyProvided;
	private String policyTerm;
	@Transient
	private Boolean liked = false;
	@ManyToOne(cascade = CascadeType.ALL)
	private User user;
	private String companyLink;
	
	public String getCompanyLink() {
		return companyLink;
	}
	public void setCompanyLink(String companyLink) {
		this.companyLink = companyLink;
	}
	public Integer getInsuranceId() {
		return insuranceId;
	}
	public void setInsuranceId(Integer insuranceId) {
		this.insuranceId = insuranceId;
	}
	public String getInsuranceName() {
		return insuranceName;
	}
	public void setInsuranceName(String insuranceName) {
		this.insuranceName = insuranceName;
	}
	public String getAgeGroup() {
		return ageGroup;
	}
	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}
	public String getSalaryGroup() {
		return salaryGroup;
	}
	public void setSalaryGroup(String salaryGroup) {
		this.salaryGroup = salaryGroup;
	}
	public Integer getPremiumPerMonth() {
		return premiumPerMonth;
	}
	public void setPremiumPerMonth(Integer premiumPerMonth) {
		this.premiumPerMonth = premiumPerMonth;
	}
	public String getCompanyUrl() {
		return companyUrl;
	}
	public void setCompanyUrl(String companyUrl) {
		this.companyUrl = companyUrl;
	}
	public String getCompanyProvided() {
		return companyProvided;
	}
	public void setCompanyProvided(String companyProvided) {
		this.companyProvided = companyProvided;
	}
	public String getPolicyTerm() {
		return policyTerm;
	}
	public void setPolicyTerm(String policyTerm) {
		this.policyTerm = policyTerm;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public Boolean getLiked() {
		return liked;
	}
	public void setLiked(Boolean liked) {
		this.liked = liked;
	}

	public static Comparator<Insurance> premiumPerMonthComparator = new Comparator<Insurance>(){

		@Override
		public int compare(Insurance o1, Insurance o2) {
			// TODO Auto-generated method stub
			return o1.premiumPerMonth-o2.premiumPerMonth;
		}
		
	};
	
		
}