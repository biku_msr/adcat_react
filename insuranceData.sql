-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: insurance
-- ------------------------------------------------------
-- Server version	5.7.14-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `insurance`
--

DROP TABLE IF EXISTS `insurance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insurance` (
  `insurance_id` int(11) NOT NULL AUTO_INCREMENT,
  `age_group` varchar(255) DEFAULT NULL,
  `company_provided` varchar(255) DEFAULT NULL,
  `company_url` varchar(255) DEFAULT NULL,
  `insurance_name` varchar(255) DEFAULT NULL,
  `policy_term` varchar(255) DEFAULT NULL,
  `premium_per_month` int(11) DEFAULT NULL,
  `salary_group` varchar(255) DEFAULT NULL,
  `user_user_id` int(11) DEFAULT NULL,
  `company_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`insurance_id`),
  KEY `FKiew250u3wy0uo452ppa1fgvgl` (`user_user_id`),
  CONSTRAINT `FKiew250u3wy0uo452ppa1fgvgl` FOREIGN KEY (`user_user_id`) REFERENCES `user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurance`
--

LOCK TABLES `insurance` WRITE;
/*!40000 ALTER TABLE `insurance` DISABLE KEYS */;
INSERT INTO `insurance` VALUES (1,'26-30','HDFC Life Insurance','https://www.goodmoneying.com/wp-content/uploads/2015/11/101.hdfc-life-logo.jpg','HDFC Click2Protect Plus','5-40',500,'3-5',1,'https://www.hdfclife.com/'),(2,'31-35','HDFC Life Insurance','https://www.goodmoneying.com/wp-content/uploads/2015/11/101.hdfc-life-logo.jpg','HDFC Life Sanchay','10-30',1000,'7-10',1,'https://www.hdfclife.com/'),(3,'36-40','HDFC Life Insurance','https://www.goodmoneying.com/wp-content/uploads/2015/11/101.hdfc-life-logo.jpg','HDFC SL Crest','20-40',700,'5-7',1,'https://www.hdfclife.com/'),(4,'41-45','AEGON Life Insurance','https://moneygyaan.com/wp-content/uploads/2016/09/aegon-life-insurance-policy-reviews.png','Aegon Life iTerm Plan','20-40',800,'10+',1,'https://www.aegonlife.com/'),(5,'46-50','Aviva Life Insurance','https://www.fincash.com/b/wp-content/uploads/2017/01/aviva-logo.jpg','Aviva I-Life Plan','20-40',800,'10+',1,'https://www.avivaindia.com/'),(6,'26-30','Bajaj Allianz Life Insurance','http://www.all4life.in/jobs/upload/images/Bajaj-Allianz-Logo1.jpg','Bajaj AllianziSecure','5-40',421,'3-5',1,'https://www.bajajallianz.com/Corp/new-index.jsp'),(7,'31-35','Bharti AXA Life Insurance','http://www.estrade.in/wp-content/uploads/2016/07/Life-Insurance-Plans-for-you-and-your-family-with-Bharti-Axa-Life-5655b1eb94c443debd9c.jpg','Bharti AXA eProtect Term Plan','10-30',984,'7-10',1,'https://www.bharti-axalife.com/'),(8,'36-40','LIC India','https://www.policyx.com/news/wp-content/uploads/2014/11/LIC.png','LIC AmulyaJeevan','20-40',800,'5-7',1,'https://www.licindia.in/'),(9,'41-45','LIC India','https://www.policyx.com/news/wp-content/uploads/2014/11/LIC.png','LIC New JeevanAnand','20-40',800,'10+',1,'https://www.licindia.in/'),(10,'46-50','LIC India','https://www.policyx.com/news/wp-content/uploads/2014/11/LIC.png','LIC Term Plan','20-40',800,'10+',1,'https://www.licindia.in/'),(11,'26-30','SBI Life Insurance','http://image3.mouthshut.com/images/imagesp/925062223s.jpg','SBI eShield Plan','5-40',475,'3-5',1,'https://www.sbilife.co.in/'),(12,'31-35','SBI Life Insurance','http://image3.mouthshut.com/images/imagesp/925062223s.jpg','SBI ShubhNivesh Plan Life Insurance','10-30',900,'7-10',1,'https://www.sbilife.co.in/'),(13,'36-40','HDFC Life Insurance','https://www.goodmoneying.com/wp-content/uploads/2015/11/101.hdfc-life-logo.jpg','HDFC Click2Protect Plus','20-40',670,'5-7',1,'https://www.hdfclife.com/'),(14,'41-45','HDFC Life Insurance','https://www.goodmoneying.com/wp-content/uploads/2015/11/101.hdfc-life-logo.jpg','HDFC Life Sanchay','20-40',800,'10+',1,'https://www.hdfclife.com/'),(15,'46-50','HDFC Life Insurance','https://www.goodmoneying.com/wp-content/uploads/2015/11/101.hdfc-life-logo.jpg','HDFC SL Crest','20-40',800,'10+',1,'https://www.hdfclife.com/'),(16,'26-30','Bharti AXA Life Insurance','http://www.estrade.in/wp-content/uploads/2016/07/Life-Insurance-Plans-for-you-and-your-family-with-Bharti-Axa-Life-5655b1eb94c443debd9c.jpg','Bharti AXA eProtect Term Plan','5-40',469,'3-5',1,'https://www.bharti-axalife.com/'),(17,'31-35','LIC India','https://www.policyx.com/news/wp-content/uploads/2014/11/LIC.png','LIC AmulyaJeevan','10-30',850,'7-10',1,'https://www.licindia.in/'),(18,'36-40','LIC India','https://www.policyx.com/news/wp-content/uploads/2014/11/LIC.png','LIC New JeevanAnand','20-40',800,'5-7',1,'https://www.licindia.in/'),(19,'41-45','LIC India','https://www.policyx.com/news/wp-content/uploads/2014/11/LIC.png','LIC Term Plan','20-40',800,'10+',1,'https://www.licindia.in/'),(20,'46-50','ICICI Prudential Life Insurance','https://www.mbaskool.com/2016_images/top_brands/india_insurance/indins16-02.jpg','ICICI Pru iProtect','20-40',800,'10+',1,'https://www.iciciprulife.com/'),(21,'26-30','ICICI Prudential Life Insurance','https://www.mbaskool.com/2016_images/top_brands/india_insurance/indins16-02.jpg','ICICI Pru iProtect','5-40',498,'3-5',1,'https://www.iciciprulife.com/'),(22,'31-35','ICICI Prudential Life Insurance','https://www.mbaskool.com/2016_images/top_brands/india_insurance/indins16-02.jpg','ICICI Pru iProtect','10-30',897,'7-10',1,'https://www.iciciprulife.com/'),(23,'36-40','ICICI Prudential Life Insurance','https://www.mbaskool.com/2016_images/top_brands/india_insurance/indins16-02.jpg','ICICI Pru iProtect','20-40',800,'5-7',1,'https://www.iciciprulife.com/'),(24,'41-45','Max Life Insurance','http://www.afaqs.com/all/news/images/news_story_grfx/2016/01/46940/Max-Life-Insurance-New-Logo.jpg','Max Life Online Term Plan','20-40',720,'10+',1,'https://www.maxlifeinsurance.com/'),(25,'46-50','Max Life Insurance','http://www.afaqs.com/all/news/images/news_story_grfx/2016/01/46940/Max-Life-Insurance-New-Logo.jpg','Max Life Online Term Plan','20-40',800,'10+',1,'https://www.maxlifeinsurance.com/'),(26,'20-25','AEGON Life Insurance','https://moneygyaan.com/wp-content/uploads/2016/09/aegon-life-insurance-policy-reviews.png','Aegon Life iTerm Plan','10-60',789,'5-7',1,'https://www.aegonlife.com/'),(27,'20-25','Max Life Insurance','http://www.afaqs.com/all/news/images/news_story_grfx/2016/01/46940/Max-Life-Insurance-New-Logo.jpg','Max Life Online Term Plan','20-40',700,'3-5',1,'https://www.maxlifeinsurance.com/'),(28,'20-25','ICICI Prudential Life Insurance','https://www.mbaskool.com/2016_images/top_brands/india_insurance/indins16-02.jpg','ICICI Pru iProtect','10-30',1000,'7-10',1,'https://www.iciciprulife.com/'),(29,'51-55','AEGON Life Insurance','https://moneygyaan.com/wp-content/uploads/2016/09/aegon-life-insurance-policy-reviews.png','Aegon Life iTerm Plan','5-10',2000,'5-7',1,'https://www.aegonlife.com/'),(30,'51-55','Max Life Insurance','http://www.afaqs.com/all/news/images/news_story_grfx/2016/01/46940/Max-Life-Insurance-New-Logo.jpg','Max Life Online Term Plan','5-15',2500,'7-10',1,'https://www.aegonlife.com/'),(31,'51-55','ICICI Prudential Life Insurance','https://www.mbaskool.com/2016_images/top_brands/india_insurance/indins16-02.jpg','ICICI Pru iProtect','5-7',3000,'10+',1,'https://www.iciciprulife.com/');
/*!40000 ALTER TABLE `insurance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `liked_insurance` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `unique_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,NULL,NULL,NULL),(2,'1,2,6,11,13,21','Subrat Mishra','1604924062900790');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-17  1:16:51
